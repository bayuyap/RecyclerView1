package id.sch.smktelkom_mlg.recyclerview1;

import android.graphics.drawable.Drawable;

/**
 * Created by SMK TELKOM on 2/13/2018.
 */

public class Hotel {
    public String judul, deskripsi;
    public Drawable foto;

    public Hotel(String judul, String deskripsi, Drawable foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.foto = foto;
    }
}
